import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * Created by tritoon on 22/08/16.
 */
public class PrinterNeighbors {
    public static void print(List<Particle> particles, String outputFile, double totalTime,
                             Map<Particle, Set<Particle>> neighborMap) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(outputFile, "UTF-8");

            for (Particle p : particles) {
                writer.print(String.format("%d ", p.getId()));
                for (Particle n : neighborMap.get(p)) {
                    writer.print(String.format("%d ", n.getId()));
                }
                writer.println();
            }
            writer.print(String.format("\n\nTotal time in ms: %f",totalTime));
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.close();
    }
}
