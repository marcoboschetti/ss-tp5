import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;

/**
 * Created by tritoon on 22/08/16.
 */
public class PrinterXYZ {

    protected static PrintWriter writer = null;

    public static void startPrinting(String fileName) {
        try {
            writer = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prints a simulation state, printing for each particle:
     *  ID RADIUS X Y VELOCITYX VELOCITYY R G B vecR vecG vecB
     * @param particles
     * @param corners
     * @param timeStep
     */

    public static void printTimeStep(List<Particle> particles, List<Wall> corners, double timeStep) {

        writer.println(particles.size() + corners.size()*2);
        writer.println(String.format("Time:%f", timeStep));

        for (Particle p : particles) {
            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f 0", p.getId(), p.getRadius(),
                    p.getX(), p.getY(),p.getVelX(), p.getVelY(), p.getColor().getR(), p.getColor().getG(), p.getColor().getB(),
                    p.getColor().getR(), p.getColor().getG(), p.getColor().getB()));
        }

        for (Wall p : corners) {
            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f %d", p.getId1(), 0.0,
                    p.getCorner1().getX(), p.getCorner1().getY(),0.0,0.0, 0.0,0.0,0.0,0.0,0.0,0.0, p.getId1()));

            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f %d", p.getId2(), 0.0,
                    p.getCorner2().getX(), p.getCorner2().getY(),0.0,0.0, 0.0,0.0,0.0,0.0,0.0,0.0, p.getId1()));
        }

    }




    public static void endPrinting() {
        writer.close();
        writer = null;
    }

    public static void printTimeStepColored(List<Particle> particles, List<Wall> corners, double timeStep) {
        writer.println(particles.size() + corners.size()*2);
        writer.println(String.format("Time:%f", timeStep));

        for (Particle p : particles) {
            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f 0", p.getId(), p.getRadius(),
                    p.getX(), p.getY(),p.getVelX(), p.getVelY(), p.getColor().getR(), p.getColor().getG(), p.getColor().getB(),
                    p.getColor().getR(), p.getColor().getG(), p.getColor().getB(),p.getColor2().getR(), p.getColor2().getG(), p.getColor2().getB()));
        }

        for (Wall p : corners) {
            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %d", p.getId1(), 0.0,
                    p.getCorner1().getX(), p.getCorner1().getY(),0.0,0.0, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0, p.getId1()));

            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %d", p.getId2(), 0.0,
                    p.getCorner2().getX(), p.getCorner2().getY(),0.0,0.0, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0, p.getId1()));
        }
    }
}
