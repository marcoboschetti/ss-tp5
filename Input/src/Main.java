import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by tritoon on 10/08/16.
 */
public class Main {

    private static final double L = 5, W = 1;
    private static final double D = W / 5;
    private static final double mass = 0.01;
    private static final int particles = 250;
    private static final double siloHeight = 1;
    private static final String staticInput = "staticStage";
    private static final String dynamicInput = "dynamicStage";

    private static final double KN = Math.pow(10, 5), KT = 2 * KN;
    private static final double timeDelta = 0.05 * Math.sqrt(mass / KN),
            printDelta = 1000 * timeDelta,
            simulationTime = 15;

    private static final String fileXYZ = "estatistics2_0.125PI";

    public static void main(String[] args) throws IOException {

        // while(new StateGenerator().generateState(W,L,particles,D/14, D/10, D, mass, staticInput,dynamicInput, siloHeight) != -1);
        // new Simulator().simulate(staticInput, dynamicInput, timeDelta, printDelta, 5, fileXYZ, KN, KT, L + siloHeight, W);


        double[] Ls = new double[]{1};


        PrintWriter writer = null;
        try {
            writer = new PrintWriter("Estadistics", "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        for (double L : Ls) {
            System.out.println("TRY: " + L);
            writer.write(String.format("\nL:%f,Particles:%n,W:%f,D:%f,printDelta:%f, timeDelta:%f\n caudal, Ek", L, particles * L, W, D,
                    printDelta, timeDelta));
            while (new StateGenerator().generateState(W, L, 0, D / 14, D / 10, D, mass, staticInput, dynamicInput, siloHeight, Math.PI / 8) != -1)
                ;

            Wall gate = new Wall(new Vector((W - D) / 2, siloHeight), new Vector((W - D) / 2 + D, siloHeight));


            List<double[]> ans = new Simulator().simulateInjection(staticInput, dynamicInput, timeDelta, printDelta, simulationTime, fileXYZ + "L=" + L, KN,
                    KT, L + siloHeight, W, printDelta * 1, particles * L + 100, D / 14, D / 10, mass, 3, gate);

            for (double[] v : ans) {
                writer.write(String.format("%f,%f", v[0], v[1]));
            }

        }

        writer.close();
    }

}