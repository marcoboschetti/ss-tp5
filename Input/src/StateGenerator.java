import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tritoon on 11/08/16.
 */
public class StateGenerator {

    private static final double COLISION_DELTA = 0.00001;
    private static final double MAX_TRIES_PER_PARTICLE = 1000;

    public int generateState(double W, double L, int N, double minRadius, double maxRadius, double D,
                             double mass, String staticFilename, String dynamicFilename, double siloHeight, double angle) {


        List<Particle> particles = new ArrayList<>(N);
        List<Wall> walls = new ArrayList<>(6);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(staticFilename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

//        walls.add(new Wall(new Vector(0,L+siloHeight),new Vector(0,0+siloHeight)));
//        walls.add(new Wall(new Vector(W,L+siloHeight),new Vector(W,0+siloHeight)));
//        walls.add(new Wall(new Vector(0,0+siloHeight),new Vector((W-D)/2,0+siloHeight)));
//        walls.add(new Wall(new Vector(D+(W-D)/2,0+siloHeight),new Vector(W,0+siloHeight)));

        double offset = Math.atan(angle) * (D + (W - D) / 2);

        walls.add(new Wall(new Vector(0, L + siloHeight), new Vector(0, offset + siloHeight)));
        walls.add(new Wall(new Vector(W, L + siloHeight), new Vector(W, offset + siloHeight)));
        walls.add(new Wall(new Vector(0, L + siloHeight), new Vector(W, L + siloHeight)));
        walls.add(new Wall(new Vector(0, offset + siloHeight), new Vector((W - D) / 2, 0 + siloHeight)));
        walls.add(new Wall(new Vector(D + (W - D) / 2, 0 + siloHeight), new Vector(W, offset + siloHeight)));

        writer.write(String.format("Walls: x1, y1, x2, y2\n%d\n", walls.size()));

        for (Wall w : walls) {
            writer.write(String.format("%f,%f,%f,%f\n ", w.getCorner1().getX(), w.getCorner1().getY(),
                    w.getCorner2().getX(), w.getCorner2().getY()));
        }

        writer.write("Particles\n");
        for (int i = 0; i < N; i++) {
            double radius = minRadius + Math.random() * (maxRadius - minRadius);

            writer.write(String.format("%f", radius));
            particles.add(i, new Particle(-1, -1, radius));
            if (i < N - 1) {
                writer.write(",");
            }
        }
        writer.close();

        try {
            writer = new PrintWriter(dynamicFilename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < N; i++) {
            boolean put = false;
            for (int tries = 0; !put && tries < MAX_TRIES_PER_PARTICLE; tries++) {
                double x = (Math.random() * (W - 2 * COLISION_DELTA - 2 * maxRadius)) + COLISION_DELTA + maxRadius;
                double y = (Math.random() * (L - 2 * COLISION_DELTA - 2 * maxRadius)) + COLISION_DELTA + siloHeight + maxRadius;

                if (!hasColision(i, x, y, particles, particles.get(i).getRadius(), W, L)) {
                    put = true;
                    particles.get(i).setX(x);
                    particles.get(i).setY(y);
                    writer.write(String.format("%f %f %f %f %f\n", x, y, 0.0, 0.0, mass));
                }
            }
            if (!put) {
                System.out.println((String.format("I can't create that, Im solid =/ I did put %d particles...", i)));
                writer.close();
                return i;
            }
        }
        writer.close();
        return -1;
    }


    double colisionDelta = 0.01;

    private boolean hasColision(int currentIndex, double x, double y, List<Particle> particles, double radius,
                                double W, double L) {
        Particle curP = particles.get(currentIndex);

        for (int i = 0; i < currentIndex; i++) {
            Particle checking = particles.get(i);
            if (Math.pow(x - checking.getX(), 2) + Math.pow(y - checking.getY(), 2) <
                    Math.pow(curP.getRadius() + checking.getRadius() + COLISION_DELTA, 2)) {
                return true;
            }

            if (!(x - radius - colisionDelta > 0 && x + radius + colisionDelta < W &&
                    y - radius - colisionDelta > 0 && y + radius + colisionDelta < L)) {
                return true;
            }

        }


        return false;
    }


}