import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by tritoon on 12/08/16.
 */
public class Color {
    private double R, G, B;

    public Color(double r, double g, double b) {
        R = r;
        G = g;
        B = b;
    }

    public Color() {
        R = 0.5;
        G = 0.5;
        B = 0.5;
    }

    public double getR() {
        return R;
    }

    public double getG() {
        return G;
    }

    public double getB() {
        return B;
    }

    public void setR(double r) {
        R = r;
    }

    public void setG(double g) {
        G = g;
    }

    public void setB(double b) {
        B = b;
    }


    /**
     * Sets a color for each particle as a heat map, based on the number of neighbors and a global scale.
     * The color is a linear interpolation between green and red in its hexa max values.
     *
     * @param particles
     */
    public static void setHeatColor(Particle particles, double maxAffected, double affected) {
        if (maxAffected == 0) {
            return;
        }

        particles.getColor().setR((affected / maxAffected) * 3);
        particles.getColor().setB(0);
        particles.getColor().setG((1 - (affected / maxAffected)));
    }

    public static void setLeposColor(Particle particles, double maxAffected, double affected) {
        double ratio = affected / maxAffected;
        particles.getColor2().setR(3 - 3 * ratio);
        particles.getColor2().setB(ratio * 3);
        particles.getColor2().setG(0);
    }

    public static void setRottingColor(Grain grain) {
        double age = grain.getAge();
        grain.getColor2().setR(0);
        grain.getColor2().setB(0);
        grain.getColor2().setG(3 - Math.min(age / 3, 3));
    }

    public static void paintPool(List<Particle> particles) {
        ArrayList<Color> colors = new ArrayList<>(16);
        colors.add(new Color(1, 0.1, 0.1));
        colors.add(new Color(0.3, 1, 0.3));
        colors.add(new Color(0.4, 0.1, 0.6));
        colors.add(new Color(0.2, 0.8, 0.8));
        colors.add(new Color(0.0, 0.0, 0.0));
        colors.add(new Color(0.0, 0.5, 0.6));
        colors.add(new Color(0.6, 0.3, 0.8));
        colors.add(new Color(0.2, 0.1, 0.7));
        colors.add(new Color(0.9, 0.0, 0.7));
        colors.add(new Color(0.4, 0.0, 0.1));
        colors.add(new Color(0.4, 0.0, 0.8));
        colors.add(new Color(0.7, 0.9, 0.2));
        colors.add(new Color(0.0, 0.7, 0.6));
        colors.add(new Color(0.5, 0.5, 0.8));
        colors.add(new Color(0.3, 0.5, 0.6));
        colors.add(new Color(1, 1, 1));

        for (int i = 0; i < 16; i++) {
            particles.get(i).setColor(colors.get(i));
        }

        particles.get(15).setMass(1.5);

    }

    public static void paintGreenBlue(List<Particle> particles, double W) {
        for (Particle p : particles) {
            if (p.getX() < W / 2) {
                p.setColor(0, 1, 0);
            } else {
                p.setColor(0, 0, 1);
            }
        }
    }

    public static void setPressureColor(Particle p, double maxAffected, Double sum) {
        if (maxAffected == 0) {
            return;
        }

        p.getColor().setR((sum / maxAffected) * 3);
        p.getColor().setB(0);
        p.getColor().setG((1 - (sum / maxAffected)));
    }
}
