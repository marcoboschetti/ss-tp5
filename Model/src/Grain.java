/**
 * Created by juan on 18/10/16.
 */
public class Grain extends Particle{

    private static final double ROT_CONSTANT = 3;
    private double age;
    private double wholePressure;

    public Grain(double x, double y, double velx, double vely, double radius, double mass) {
        super(x, y, velx, vely, radius, mass);
        age = 0;
        wholePressure = 0;
    }

    public boolean age(double amount){
        age += amount;
        return age > ROT_CONSTANT;
    }

    public double getAge(){
        return age;
    }

    public double getWholePressure() {
        return wholePressure;
    }

    public void setWholePressure(double wholePressure) {
        this.wholePressure = wholePressure;
    }

    public void resetAge() {
        this.age = 0;
    }
}
