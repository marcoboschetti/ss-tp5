import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by tritoon on 19/09/16.
 */
public interface ForceCalculator {
    public void calculateForce(List<Particle> particles, List<Wall> walls, Map<Particle, Set<Particle>> neighborMap, List<Vector> forces) ;
}
