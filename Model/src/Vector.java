import java.awt.geom.Point2D;

/**
 * Created by tritoon on 19/09/16.
 */
public class Vector {
    private double x;
    private double y;

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public synchronized void add(double x, double y) {
        this.x += x;
        this.y += y;
    }

    public Vector sub(Vector other) {
        return new Vector(this.x - other.x, this.y - other.y);
    }

    public double dotProduct(Vector other) {
        return this.x * other.x + this.y * other.y;
    }

    public Vector scalarProduct(double num) {
        return new Vector(this.x * num, this.y * num);
    }

    public Vector add(Vector vector) {
        return new Vector(this.x + vector.x, this.y + vector.y);
    }

    public double getModule() {
        return Math.hypot(x, y);
    }
}
