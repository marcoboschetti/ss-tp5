/**
 * Created by juan on 12/10/16.
 */
public class FixedParticle extends Particle{

    public FixedParticle(double x, double y) {
        super(x, y, 0, 0, 0, Double.MAX_VALUE);
    }

    public FixedParticle(Vector position) {
        super(position.getX(), position.getY(), 0, 0, 0, Double.MAX_VALUE);
    }

    public void setVelX(double velX){

    }

    public void setVelY(double velY){

    }

    public void setX(double x){

    }

    public void setY(double y){

    }



}
