/**
 * Created by tritoon on 05/10/16.
 */
public class Wall {

    private Vector corner1, corner2, direction;
    /*
    etx and ety are considered from corner1 to corner2
     */
    private double topLim, bottomLim, leftLim, rightLim, length, xDifference, yDifference,
            crossProductDifference;
    private int id1, id2;

    public Wall(Vector corner1, Vector corner2) {
        this.corner1 = corner1;
        this.corner2 = corner2;
        topLim = Math.max(corner1.getY(), corner2.getY());
        bottomLim = Math.min(corner1.getY(), corner2.getY());
        leftLim = Math.min(corner1.getX(), corner2.getX());
        rightLim = Math.max(corner1.getX(), corner2.getX());
        this.id1 = Particle.getNextId();
        this.id2 = Particle.getNextId();
        length = Math.hypot(corner2.getX() - corner1.getX(), corner2.getY() - corner1.getY());
        xDifference = corner2.getX() - corner1.getX();
        yDifference = corner2.getY() - corner1.getY();
        crossProductDifference = corner2.getX() * corner1.getY() - corner2.getY() * corner1.getX();
        double etx = (corner2.getX() - corner1.getX()) / length;
        double ety = (corner2.getY() - corner1.getY()) / length;
        direction = new Vector(etx, ety);
    }

    public Vector getCorner1() {
        return corner1;
    }

    public Vector getCorner2() {
        return corner2;
    }

    public double getTopLim() {
        return topLim;
    }

    public double getBottomLim() {
        return bottomLim;
    }

    public double getLeftLim() {
        return leftLim;
    }

    public double getRightLim() {
        return rightLim;
    }

    public int getId1() {
        return id1;
    }

    public int getId2() {
        return id2;
    }

    public double getLength() {
        return length;
    }

    public double getOverlap(Particle p1) {
        double distance = Math.abs(yDifference * p1.getX() - xDifference * p1.getY() + crossProductDifference) / length;
//        Vector projectedPoint = corner1.add(direction.scalarProduct(dotProduct));

        if(p1.getRadius() > distance){
            double dotProduct = p1.getPosition().sub(corner1).dotProduct(direction);
            Vector projectedPoint = new Vector(corner1.getX() + direction.getX() * dotProduct, corner1.getY() + direction.getY() * dotProduct);
            if(projectedPoint.getX() >= leftLim && projectedPoint.getX() <= rightLim &&
                    projectedPoint.getY() >= bottomLim && projectedPoint.getY() <= topLim)
                return p1.getRadius() - distance;
        }
        return -1;
    }

    public Vector getNormal(Particle p1){
        double dotProduct = p1.getPosition().sub(corner1).dotProduct(direction);
        Vector projectedPoint = new Vector(corner1.getX() + direction.getX() * dotProduct, corner1.getY() + direction.getY() * dotProduct);
        double distance = Math.abs(yDifference * p1.getX() - xDifference * p1.getY() + crossProductDifference) / length;
        double enx = (projectedPoint.getX() - p1.getX())/distance;
        double eny = (projectedPoint.getY() - p1.getY())/distance;
        return new Vector(enx, eny);
    }

}
