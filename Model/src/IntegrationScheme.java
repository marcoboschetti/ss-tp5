import java.util.List;

/**
 * Created by tritoon on 19/09/16.
 */
public interface IntegrationScheme {


    /**
     * @param toUpdate Particle with x,y,vx,vy from the previous T
     * @param deltaT Time to update the new position and velocity
     * @param forceX new acceleration in X
     * @param forceY new acceleration in Y
     * @param matrix
     * @param sideX
     * @param sideY
     * @param l
     * @param w
     */
    void updateParticle(Particle toUpdate, double deltaT, double forceX, double forceY, List<Particle>[][] matrix, double sideX, double sideY, double l, double w);


}
