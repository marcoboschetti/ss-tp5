import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by juan on 19/09/16.
 */
public class Beeman implements IntegrationScheme {
    private final double gamma, k;
    private Map<Particle, Vector> map;

    public Beeman(double gamma, double k) {
        this.gamma = gamma;
        this.k = k;
        map = new HashMap<>();
    }

    @Override
    public void updateParticle(Particle toUpdate, double deltaT, double forceX, double forceY, List<Particle>[][] matrix, double sideX,
                               double sideY, double L, double W) {
        Vector prevA;
        if (map.containsKey(toUpdate)) {
            prevA = map.get(toUpdate);
        } else {
            Particle clone = toUpdate.clone();
            new EulerMethod().updateParticle(clone, -1 * deltaT, forceX, forceY, matrix, sideX, sideY, L, W);
            Vector prevForce = new OscilationForce(k, gamma).calculateForce(clone);
            prevA = new Vector(prevForce.getX() / toUpdate.getMass(), prevForce.getY() / toUpdate.getMass());
            map.put(toUpdate, prevA);
        }

        double newPosX = toUpdate.getX() +
                toUpdate.getVelX() * deltaT +
                2 * forceX * deltaT * deltaT / (3 * toUpdate.getMass()) +
                deltaT * deltaT * prevA.getX() / 6;
        double newPosY = toUpdate.getY() +
                toUpdate.getVelY() * deltaT +
                2 * forceY * deltaT * deltaT / (3 * toUpdate.getMass()) +
                deltaT * deltaT * prevA.getY() / 6;

        toUpdate.setX(newPosX);
        toUpdate.setY(newPosY);

        Vector newForce = new OscilationForce(k,gamma).calculateForce(toUpdate);
        Vector nextA = new Vector(newForce.getX() / toUpdate.getMass(), newForce.getY() / toUpdate.getMass());
        map.put(toUpdate, nextA);

        double newVelX = toUpdate.getVelX() +
                nextA.getX() * deltaT / 3 +
                5 * forceX * deltaT / (6 * toUpdate.getMass()) -
                prevA.getX() * deltaT / 6;
        double newVelY = toUpdate.getVelY() +
                nextA.getY() * deltaT / 3 +
                5 * forceY * deltaT / (6 * toUpdate.getMass()) -
                prevA.getY() * deltaT / 6;

        toUpdate.setVelX(newVelX);
        toUpdate.setVelY(newVelY);

    }


}
