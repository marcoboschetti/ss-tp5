import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tritoon on 19/09/16.
 */
public class LeapFrogMethod implements IntegrationScheme {

    private Map<Integer, Vector> previusHalfVel;

    public LeapFrogMethod() {
        previusHalfVel = new HashMap<>();
    }

    @Override
    public void updateParticle(Particle toUpdate, double deltaT, double forceX, double forceY,
                               List<Particle>[][] matrix, double sideX, double sideY, double L, double W) {
        Vector previousIHalfVel = null;

        if (previusHalfVel.containsKey(toUpdate.getId())) {
            previousIHalfVel = previusHalfVel.get(toUpdate.getId());
        } else {
            previousIHalfVel = new Vector(0, 0);
            previusHalfVel.put(toUpdate.getId(), previousIHalfVel);
        }

        double newHalfVelX = previousIHalfVel.getX() + deltaT * forceX / toUpdate.getMass();
        double newHalfVelY = previousIHalfVel.getY() + deltaT * forceY / toUpdate.getMass();

        double newX = toUpdate.getX() + deltaT * previousIHalfVel.getX();
        double newY = toUpdate.getY() + deltaT * previousIHalfVel.getY();

        double newVelX = newHalfVelX + previousIHalfVel.getX() / 2;
        double newVelY = newHalfVelY + previousIHalfVel.getY() / 2;


        double oldX = toUpdate.getX();
        double oldY = toUpdate.getY();

        if (newY <= 0) {
            newVelX = 0;
            newVelY = 0;
            newHalfVelX = 0;
            newHalfVelY = 0;
            Vector vector = repositionParticle(toUpdate, matrix, sideX, sideY, L, W);
            newX = vector.getX();
            newY = vector.getY();
        }

        if(newY >= (L-toUpdate.getRadius())){
            newVelY = 0;
            newHalfVelY = 0;
            newY = L - toUpdate.getRadius() * 2;

        }

        int oldcellx = (int) Math.max(0, Math.min(matrix.length - 1, oldX / sideX));
        int oldcelly = (int) Math.max(0, Math.min(matrix[0].length - 1, oldY / sideY));

        int cellx = (int) Math.max(0, Math.min(matrix.length - 1, newX / sideX));
        int celly = (int) Math.max(0, Math.min(matrix[0].length - 1, newY / sideY));

        if ((oldcellx != cellx) || (oldcelly != celly)) {
            matrix[oldcellx][oldcelly].remove(toUpdate);

            matrix[cellx][celly].add(toUpdate);
        }


        toUpdate.setX(newX);
        toUpdate.setY(newY);

        toUpdate.setVelX(newVelX);
        toUpdate.setVelY(newVelY);

        previousIHalfVel.setX(newHalfVelX);
        previousIHalfVel.setY(newHalfVelY);
    }


    private static double heightDelta = 16;
    private static double MAX_TRIES_WARNING = 500;


    public static Vector repositionParticle(Particle toUpdate, List<Particle>[][] matrix, double sideX, double sideY
            , double L, double W) {
        boolean collides = true;
        double newX = 0, newY = 0;
        double tries = MAX_TRIES_WARNING;


        while (collides) {
            newX = Math.random() * (W - toUpdate.getRadius() * 2) + toUpdate.getRadius();
            newY = L - (Math.random() * L / heightDelta) - toUpdate.getRadius();
            collides = false;

            for (int celli = (int) Math.max(0, (newX / sideX) - 1); celli < Math.min(matrix.length, (newX / sideX) + 1) && !collides; celli++) {
                for (int cellj = (int) Math.max(0, (newY / sideY) - 1); cellj < Math.min(matrix[0].length, (newY / sideY) + 1) && !collides; cellj++) {

                    List<Particle> cell = matrix[celli][cellj];

                    for (int i = 0; i < cell.size() && !collides; i++) {
                        Particle p = cell.get(i);

                        double dist = Math.hypot(p.getX() - newX, p.getY() - newY);
                        double overlap = p.getRadius() + toUpdate.getRadius() - dist;

                        if (overlap >0) {
                            tries--;
                            if (tries == 0) {
                                System.out.println(MAX_TRIES_WARNING + " times I failed to reposition. \n" +
                                        "I will keep trying, but you might need to reconsider those parameters.");
                                return null;
                               // tries = MAX_TRIES_WARNING;
                            }
                            collides = true;
                        }
                    }
                }
            }
        }

        toUpdate.setX(newX);
        toUpdate.setY(newY);
        if(toUpdate instanceof Grain){
            ((Grain) toUpdate).resetAge();
        }
        return new Vector(newX, newY);
    }

}
