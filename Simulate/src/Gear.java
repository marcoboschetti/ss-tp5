import java.util.List;

/**
 * Created by juanc on 21/9/2016.
 */
public class Gear implements IntegrationScheme {
    private double lastR, lastR1, lastR2, lastR3, lastR4, lastR5;

    private final static double FACT3 = 6,
            FACT4 = 24,
            FACT5 = 120;
    private final static double[] ALPHAS = {3/16, 251/360, 1, 11/18, 1/6, 1/60};

    private final double k;

    public Gear(Particle particle, double k, double gamma) {
        this.k = k;
        double m = particle.getMass();
        lastR = particle.getX();
        lastR1 = particle.getVelX();
        lastR2 = -k * lastR / m - gamma * lastR1 / m;
        lastR3 = -k * lastR1 / m - gamma * lastR2 / m;
        lastR4 = -k * lastR2 / m - gamma * lastR3 / m;
        lastR5 = -k * lastR3 / m - gamma * lastR4 / m;

    }

    @Override
    public void updateParticle(Particle toUpdate, double deltaT, double forceX, double forceY, List<Particle>[][] matrix, double sideX, double sideY, double l, double w) {

        double k = Math.pow(10, 4);// FIXME: 21/9/2016
        double gamma = 100;
        OscilationForce calculator = new OscilationForce(k, gamma);

        double m = toUpdate.getMass();
        lastR = toUpdate.getX();
        lastR1 = toUpdate.getVelX();
        lastR2 = -k * lastR / m - gamma * lastR1 / m;
        lastR3 = -k * lastR1 / m - gamma * lastR2 / m;
        lastR4 = -k * lastR2 / m - gamma * lastR3 / m;
        lastR5 = -k * lastR3 / m - gamma * lastR4 / m;


        double r = lastR + lastR1 * deltaT +
                lastR2 * Math.pow(deltaT, 2) / 2 +
                lastR3 * Math.pow(deltaT, 3) / FACT3 +
                lastR4 * Math.pow(deltaT, 4) / FACT4 +
                lastR5 * Math.pow(deltaT, 5) / FACT5,
                r1 = lastR1 + lastR2 * deltaT +
                        lastR3 * Math.pow(deltaT, 2) / 2 +
                        lastR4 * Math.pow(deltaT, 3) / FACT3 +
                        lastR5 * Math.pow(deltaT, 4) / FACT4,
                r2 = lastR2 + lastR3 * deltaT +
                        lastR4 * Math.pow(deltaT, 2) / 2 +
                        lastR5 * Math.pow(deltaT, 3) / FACT3;
//                r3 = lastR3 + lastR4 * deltaT +
//                        lastR5 * Math.pow(deltaT, 2) / 2,
//                r4 = lastR4 + lastR5 * deltaT,
//                r5 = lastR5;

        toUpdate.setX(r);
        toUpdate.setVelX(r1);

        double nextA = calculator.calculateForce(toUpdate).getX() / m;
        double deltaA = nextA - r2;
        double deltaR2 = deltaA * deltaT * deltaT/2;

        lastR = r + ALPHAS[0] * deltaR2;
        lastR1 = r1 + ALPHAS[1] * deltaR2 / deltaT;
//        lastR2 = r2 + ALPHAS[2] * deltaR2 * 2 / Math.pow(deltaT, 2);
//        lastR3 = r3 + ALPHAS[3] * deltaR2 * FACT3 / Math.pow(deltaT, 3);
//        lastR4 = r4 + ALPHAS[4] * deltaR2 * FACT4 / Math.pow(deltaT, 4);
//        lastR5 = r5 + ALPHAS[5] * deltaR2 * FACT5 / Math.pow(deltaT, 5);

        toUpdate.setX(lastR);
        toUpdate.setVelX(lastR1);

        // TODO: 21/9/2016  

//                deltaR2 = r2 -


//        Δa = Δr2 = a(t+Δt) - ap(t+Δt) = r2(t+Δt) - r2p(t+Δt)
//
//        r2 = -k/m (r-r0)
//        r3 = -k/m r1 ;
//        r4 = -k/m r2 = (k/m) 2 (r-r0);
//        r5 = -k/m r3 = (k/m) 2 r1
    }
}
