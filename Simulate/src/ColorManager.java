import java.util.*;

/**
 * Created by tritoon on 22/08/16.
 */
public class ColorManager {

    private static int QUEUE_SIZE = 0;

    private static Map<Particle, LinkedList<Double>> pressures;

    public static void setHeatColor(Map<Particle, Set<Particle>> neighborMap, List<Particle> particles) {
        int maxAffected = 0;
        for (Set l : neighborMap.values()) {
            if (l.size() > maxAffected) {
                maxAffected = l.size();
            }
        }
        for (Particle p : particles) {
            Color.setHeatColor(p, maxAffected, neighborMap.get(p).size());
        }
    }


    private static Color[][] matrixColor;

    public static void setMatrixColor(List<Particle>[][] matrix) {

        if (matrixColor == null) {
            matrixColor = new Color[matrix.length][matrix[0].length];
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    matrixColor[i][j] = new Color(Math.random() * 2, Math.random() * 2, Math.random() * 2);
                }
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                for (Particle p : matrix[i][j]) {
                    p.setColor(matrixColor[i][j]);
                }
            }
        }

    }


    public static void setSpeedColor(List<Particle> particles) {
        double maxAffected = 0;
        for (Particle p : particles) {
            if (speed(p) > maxAffected) {
                maxAffected = speed(p);
            }
        }
        for (Particle p : particles) {
            Color.setHeatColor(p, maxAffected, speed(p));
        }
    }

//    public static void setPressureColor(Map<Particle, Vector> forces) {
//        double maxAffected = 0;
//        for (Vector force : forces.values()) {
//            double module = force.getModule();
//            if (module > maxAffected) {
//                maxAffected = module;
//            }
//        }
//        for (Map.Entry<Particle, Vector> entry : forces.entrySet()) {
//            Color.setLeposColor(entry.getKey(), maxAffected, entry.getValue().getModule());
//        }
//    }

    private static double speed(Particle p) {
        return Math.pow(p.getVelX() * p.getVelX() + p.getVelY() * p.getVelY(), 0.5);
    }


    // This was created in every pressureColor(); the overhead was huge!
   private static Map<Particle, Double> sums = new HashMap<>();

    public static void initializePressures(List<Particle> grains, int window) {
        QUEUE_SIZE = window;

        if(pressures == null) {
            pressures = new HashMap<>();
        }
        for (Particle particle : grains) {
            if(particle instanceof Grain){
                if (!pressures.containsKey(particle)) {
                    LinkedList<Double> aux = new LinkedList<>();
                    pressures.put(particle, aux);
                    for(int i = 0; i < QUEUE_SIZE; i++){
                        aux.add(0.0);
                    }
                }
            }
        }

    }


    public static void addPressure(List<Particle> grains) {
        for (Particle particle: grains) {
            if(particle instanceof Grain){
                LinkedList<Double> list = pressures.get(particle);
                if(list == null){
                    list = new LinkedList<>();
                    pressures.put(particle, list);
                    for(int i = 0; i < QUEUE_SIZE; i++){
                        list.add(0.0);
                    }
                    list = pressures.get(particle);
                }
                list.poll();
                list.offer(((Grain) particle).getWholePressure());
            }
        }
    }

    public static void rottingColor(List<Particle> particles){
        for(Particle particle: particles){
            if(particle instanceof Grain){
                Color.setRottingColor((Grain)particle);
            }
        }
    }

    public static void pressureColor() {
        sums.clear();
        double maxAffected = 0.0;
        for (Map.Entry<Particle, LinkedList<Double>> entry : pressures.entrySet()) {
            Double sum = entry.getValue().stream().reduce(0.0, (x, y) -> x + y);
            sums.put(entry.getKey(), sum);
            if (sum > maxAffected) {
                maxAffected = sum;
            }
        }
        for (Map.Entry<Particle, Double> entry : sums.entrySet()) {
//            Color.setPressureColor(p, maxAffected, sums.get(p));
            Color.setLeposColor(entry.getKey(), maxAffected, entry.getValue());
        }
    }

    public static void setAngleColor(List<Particle> particles) {
        for (Particle p : particles) {
            p.setColor(0.5, (Math.cos(p.getVelAngle()) + 1) / 2, (Math.sin(p.getVelAngle()) + 1) / 2);
        }
    }

    private static void highlight(Optional<Particle> particle, Map<Particle, Set<Particle>> neighborMap) {
        if (particle.isPresent()) {
            particle.get().setColor(new Color(0, 0, 1));
            Set<Particle> neighbors = neighborMap.get(particle.get());
            neighbors.forEach(x -> x.setColor(new Color(238.0 / 255, 130.0 / 255, 238.0 / 255)));
        } else {
            System.out.println("Particle not found");
        }
    }

    public static void pressureColor(List<Particle> particles, Map<Particle, Integer> pressures) {
        int maxAffected = 0;
        for (int i : pressures.values()) {
            if (i > maxAffected) {
                maxAffected = i;
            }
        }

        for (Particle p : particles) {
            Color.setHeatColor(p, maxAffected, pressures.get(p));
        }
    }

    public static void marsLanding(List<Particle> p) {
        //Earth
        p.get(0).setColor(0, 2, 0);
        //Sun
        p.get(1).setColor(1, 1, 0);
        //Mars
        p.get(2).setColor(1, 0.2, 0);

    }
}
