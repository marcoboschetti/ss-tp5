import java.util.List;

/**
 * Created by tritoon on 19/09/16.
 */
public class EulerMethod implements IntegrationScheme {


    /**
     * Modified Euler Method implementation
     * @param toUpdate Particle with x,y,vx,vy from the previous T
     * @param deltaT Time to update the new position and velocity
     * @param forceX new acceleration in X
     * @param forceY new acceleration in Y
     * @param matrix
     * @param sideX
     * @param sideY
     * @param L
     * @param W
     */
    @Override
    public void updateParticle(Particle toUpdate, double deltaT, double forceX, double forceY, List<Particle>[][] matrix,
                               double sideX, double sideY, double L, double W) {

        double newX = toUpdate.getX() + deltaT * toUpdate.getVelX() + deltaT*deltaT * forceX / (2*toUpdate.getMass());
        double newY = toUpdate.getY() + deltaT * toUpdate.getVelY() + deltaT*deltaT * forceY / (2*toUpdate.getMass());

        double newVelX = toUpdate.getVelX() + deltaT * forceX / toUpdate.getMass();
        double newVelY = toUpdate.getVelY() + deltaT * forceY / toUpdate.getMass();

        toUpdate.setVelX(newVelX);
        toUpdate.setVelY(newVelY);
        toUpdate.setX(newX);
        toUpdate.setY(newY);
    }

}
