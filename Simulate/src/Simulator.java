import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by tritoon on 10/08/16.
 */


public class Simulator {

    private static final boolean PRINTING = true;
    private static final int QUEUE_FACTOR = 2;

    private List<Particle> grains;
    private List<Wall> walls;
    private Map<Particle, Set<Particle>> neighborMap;


    public List<double[]> simulate(String staticInput, String dynamicInput, double timeDelta,
                                   double printDelta, double totalDurationSeconds,
                                   String fileXYZ, double KN, double KT, double L, double W) {

        GranularForce f = new GranularForce(KN, KT);

        Map<Particle, Vector> forces = new HashMap<>();

        LeapFrogMethod integration = new LeapFrogMethod();

        walls = new ArrayList<>();
        grains = new ArrayList<>();

        // change maxRadius with MaxVel?
        double maxRadius = 0.4; //initializeStage(staticInput, dynamicInput);


        if (PRINTING) {
            ColorManager.initializePressures(grains, (int) (QUEUE_FACTOR * (printDelta / timeDelta)));

            PrinterXYZ.startPrinting(String.format("%s.xyz", fileXYZ));
            PrinterXYZ.printTimeStep(grains, walls, 0);

        }

        for (Particle p : grains) {
            forces.put(p, new Vector(0, 0));
        }

        double timerCounter = 0;

        int MX = (int) Math.floor(W / (2 * maxRadius));
        int MY = (int) Math.floor(L / (2 * maxRadius));

        System.out.println("mx: " + MX + " , my: " + MY);

        List<Particle>[][] matrix = new LinkedList[MX + 1][MY + 1];
        for (int i = 0; i < MX + 1; i++) {
            for (int j = 0; j < MY + 1; j++) {
                matrix[i][j] = new LinkedList<>();
            }
        }

        double sideX = W / MX;
        double sideY = L / MY;

        for (Particle particle : grains) {

            matrix[(int) Math.floor(particle.getX() / sideX)][(int) Math.floor(particle.getY() / sideY)].add(particle);
        }


        for (double totalTime = 0; totalTime < totalDurationSeconds; totalTime += timeDelta) {

            checkInfluence(matrix);
            f.calculateForce(grains, walls, neighborMap, forces, true);

            ColorManager.addPressure(grains);

            for (Particle p : grains) {
                Vector newForce = forces.get(p);
                integration.updateParticle(p, timeDelta, newForce.getX(), newForce.getY(),
                        matrix, sideX, sideY, L, W);
//                if(p instanceof Grain){
//                    ((Grain) p).age(timeDelta);
//                }
            }

            timerCounter += timeDelta;

            if (timerCounter >= printDelta) {
                if (PRINTING) {
//                    ColorManager.setSpeedColor(grains);
                    ColorManager.rottingColor(grains);
//                    ColorManager.pressureColor();
                    PrinterXYZ.printTimeStep(grains, walls, 0);
                    System.out.println(totalTime / totalDurationSeconds + " %");
                }
                timerCounter = 0;
            }

        }

        if (PRINTING) {
            PrinterXYZ.endPrinting();
        }


        return null;
    }


    public List<double[]> simulateInjection(String staticInput, String dynamicInput, double timeDelta,
                                            double printDelta, double totalDurationSeconds,
                                            String fileXYZ, double KN, double KT, double L, double W, double injectionDelta,
                                            double grainsN, double minRadius, double maxRadius, double mass,
                                            double injectionPerFrame, Wall gate) {


        GranularForce f = new GranularForce(KN, KT);

        Map<Particle, Vector> forces = new HashMap<>();

        LeapFrogMethod integration = new LeapFrogMethod();

        walls = new ArrayList<>();
        walls.add(gate);
        grains = new ArrayList<>();

        if (PRINTING) {
            PrinterXYZ.startPrinting(String.format("%s.xyz", fileXYZ));
            ColorManager.initializePressures(grains,(int) (QUEUE_FACTOR * (printDelta / timeDelta)));
        }

        initializeStage(staticInput, dynamicInput);
        System.out.println("WALLS: " + walls.size());
        for (Particle p : grains) {
            forces.put(p, new Vector(0, 0));
        }


        double timerCounter = 0;
        double injectionCounter = 0;

        int MX = (int) Math.floor(W / (2 * maxRadius * 6));
        int MY = (int) Math.floor(L / (2 * maxRadius * 6));

        List<Particle>[][] matrix = new LinkedList[MX + 1][MY + 1];
        for (int i = 0; i < MX + 1; i++) {
            for (int j = 0; j < MY + 1; j++) {
                matrix[i][j] = new LinkedList<>();
            }
        }

        double sideX = W / MX;
        double sideY = L / MY;

        for (Particle particle : grains) {
            matrix[(int) Math.floor(particle.getX() / sideX)][(int) Math.floor(particle.getY() / sideY)].add(particle);
        }


        double radius2 = minRadius + Math.random() * (maxRadius - minRadius);
        Particle newParticle2 = new Grain(0, 0, 0, 0, radius2, mass);
        LeapFrogMethod.repositionParticle(newParticle2, matrix, sideX, sideY, L, W);
        grains.add(newParticle2);
        matrix[(int) Math.floor(newParticle2.getX() / sideX)][(int) Math.floor(newParticle2.getY() / sideY)].add(newParticle2);
        forces.put(newParticle2, new Vector(0, 0));

        while (grains.size() < grainsN) {
            timerCounter += timeDelta;
            injectionCounter += timeDelta;

            if (injectionCounter >= injectionDelta) {
                boolean putting = true;
                for (int i = 0; i < injectionPerFrame && grains.size() < grainsN && putting; i++) {
                    double radius = minRadius + Math.random() * (maxRadius - minRadius);
                    Particle newParticle = new Grain(0, 0, 0, 0, radius, mass);
                    Vector ans = LeapFrogMethod.repositionParticle(newParticle, matrix, sideX, sideY, L, W);
                    if (ans != null){
                        grains.add(newParticle);
                        forces.put(newParticle, new Vector(0, 0));

                        int cellx = (int) Math.max(0, Math.min(matrix.length - 1, newParticle.getX() / sideX));
                        int celly = (int) Math.max(0, Math.min(matrix[0].length - 1, newParticle.getY() / sideY));

                        matrix[cellx][celly].add(newParticle);
                        injectionCounter = 0;
                    }else{
                        putting = false;
                        grainsN--;
                    }
                }
                System.out.println("Injection: " + (grains.size() / grainsN));
            }

            checkInfluence(matrix);

            f.calculateForce(grains, walls, neighborMap, forces, true);

//            ColorManager.addPressure(grains);
            for (Particle p : grains) {
                Vector newForce = forces.get(p);
                integration.updateParticle(p, timeDelta, newForce.getX(), newForce.getY(),
                        matrix, sideX, sideY, L, W);
                if(p instanceof Grain){
                    ((Grain) p).age(timeDelta);
                }
            }



            if (timerCounter >= printDelta) {
                if (PRINTING) {
//                    ColorManager.setSpeedColor(grains);
//                    ColorManager.pressureColor();
                    ColorManager.rottingColor(grains);
                    PrinterXYZ.printTimeStepColored(grains, walls, 0);
                    timerCounter = 0;
                }
            }

        }

        double maxVel = 10;
        double MAX_VEL = 0.5;

        while (maxVel >= MAX_VEL) {
            maxVel = 0;

            injectionCounter += timeDelta;
            timerCounter += timeDelta;

            checkInfluence(matrix);

            f.calculateForce(grains, walls, neighborMap, forces, true);
//            ColorManager.addPressure(grains);


            for (Particle p : grains) {
                Vector newForce = forces.get(p);
                integration.updateParticle(p, timeDelta, newForce.getX(), newForce.getY(),
                        matrix, sideX, sideY, L, W);
                double vel = Math.hypot(p.getVelX(), p.getVelY());
                if (vel > maxVel) {
                    maxVel = vel;
                }
                if(p instanceof Grain){
                    ((Grain) p).age(timeDelta);
                }
            }

            System.out.println("MAX VEL: " + maxVel);

            if (timerCounter >= printDelta) {
                if (PRINTING) {
                    timerCounter = 0;
//                    ColorManager.setSpeedColor(grains);
//                    ColorManager.pressureColor();
                    ColorManager.rottingColor(grains);
                    PrinterXYZ.printTimeStepColored(grains, walls, 0);
                }
            }

        }


        walls.remove(gate);

        return endInjection(totalDurationSeconds, timeDelta, printDelta, matrix, f, forces, integration, sideX, sideY, L, W);
    }


    private List<double[]> endInjection(double totalDurationSeconds, double timeDelta, double printDelta,
                                        List<Particle>[][] matrix, GranularForce f, Map<Particle, Vector> forces,
                                        LeapFrogMethod integration, double sideX, double sideY, double L, double W) {

        List<double[]> ans = new LinkedList<>();
        double caudal = 0;

        double timerCounter = 0, caudalCounter = 0;

        for (double totalTime = 0; totalTime < totalDurationSeconds; totalTime += timeDelta) {

            checkInfluence(matrix);

            f.calculateForce(grains, walls, neighborMap, forces, true);
//            ColorManager.addPressure(grains);

            double acuAge = 0;
            for (Particle p : grains) {
                double oldy = p.getY();

                Vector newForce = forces.get(p);
                integration.updateParticle(p, timeDelta, newForce.getX(), newForce.getY(),
                        matrix, sideX, sideY, L, W);

                if (oldy >= 1 && p.getY() < 1) {
                    caudal++;
                }


                if(p instanceof Grain){
                    ((Grain) p).age(timeDelta);
                    acuAge += ((Grain) p).getAge();
                }

            }

            timerCounter += timeDelta;
            caudalCounter += timerCounter;

            if (timerCounter >= printDelta && PRINTING) {
//                ColorManager.setSpeedColor(grains);
//                ColorManager.pressureColor();
                ColorManager.rottingColor(grains);
                PrinterXYZ.printTimeStepColored(grains, walls, 0);

                if (caudalCounter >= printDelta * 1000) {
                    double Ek = 0;
                    for (Particle p : grains) {
                        Ek += Math.pow(Math.hypot(p.getVelX(), p.getVelY()), 2) * p.getMass();
                    }

                    double[] aux = new double[]{caudal, Ek, acuAge/grains.size()};

                    ans.add(aux);

                    caudal = 0;
                    caudalCounter = 0;
                }

                System.out.println(totalTime / totalDurationSeconds + " %");
                timerCounter = 0;
            }
        }

        if (PRINTING) {
            PrinterXYZ.endPrinting();
        }

        return ans;
    }


    private double initializeStage(String staticData, String dynamicData) {

        double maxRadius = 0;
        try {
            BufferedReader staticInputBufferedReader = new BufferedReader(new FileReader(staticData));

            staticInputBufferedReader.readLine();
            int wall_parameters = Integer.valueOf(staticInputBufferedReader.readLine());
            String line[];
            for (int i = 0; i < wall_parameters; i++) {
                line = staticInputBufferedReader.readLine().split(",");
                walls.add(new Wall(new Vector(Double.parseDouble(line[0]), Double.parseDouble(line[1])),
                        new Vector(Double.parseDouble(line[2]), Double.parseDouble(line[3]))));
            }
            for (Wall wall : walls) {
                grains.add(new FixedParticle(wall.getCorner1()));
                grains.add(new FixedParticle(wall.getCorner2()));
            }

            BufferedReader dynamicInputBufferedReader = new BufferedReader(new FileReader(dynamicData));
            staticInputBufferedReader.readLine();
            String aux = staticInputBufferedReader.readLine();
            staticInputBufferedReader.close();

            if (aux != null) {
                String[] radiuses = aux.split(",");
                for (String radius : radiuses) {
                    double doubleRadius = Double.parseDouble(radius);
                    String[] position;
                    position = dynamicInputBufferedReader.readLine().split(" ");
                    double x = Double.parseDouble(position[0]);
                    double y = Double.parseDouble(position[1]);
                    double velX = Double.parseDouble(position[2]);
                    double velY = Double.parseDouble(position[3]);
                    double mass = Double.parseDouble(position[4]);
                    grains.add(new Grain(x, y, velX, velY, doubleRadius, mass));
                    if (maxRadius < doubleRadius) {
                        maxRadius = doubleRadius;
                    }
                }
            }

            dynamicInputBufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return maxRadius;
    }

    private void checkInfluence(List<Particle>[][] matrix) {

        double rc = 0;

        neighborMap = new HashMap<>();

        for (Particle p : grains) {
            neighborMap.put(p, new HashSet<>());
        }

        for (int indexX = 0; indexX < matrix.length; indexX++) {
            for (int indexY = 0; indexY < matrix[0].length; indexY++) {
                checkSelfNeighbor(matrix[indexX][indexY], rc);

                if (indexY > 0) {
                    checkNeighbor(matrix[indexX][indexY], matrix[indexX][indexY - 1], rc, 0, 0);
                    if (indexX < matrix.length - 1) {
                        checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][indexY - 1], rc, 0, 0);
                    }
                }
                if (indexX < matrix.length - 1) {
                    checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][indexY], rc, 0, 0);
                    if (indexY < matrix[0].length - 1) {
                        checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][indexY + 1], rc, 0, 0);
                    }
                }

            }
        }
    }

    private void checkSelfNeighbor(List<Particle> pList1, double rc) {
        for (int i = 0; i < pList1.size(); i++) {
            Particle p1 = pList1.get(i);
            for (int j = i + 1; j < pList1.size(); j++) {
                Particle p2 = pList1.get(j);
                evaluateParticles(p1, p2, rc, 0, 0);
            }
        }
    }

    private void checkNeighbor(List<Particle> pList1, List<Particle> pList2, double rc, double XShift, double YShift) {
        for (Particle p1 : pList1) {
            for (Particle p2 : pList2) {
                evaluateParticles(p1, p2, rc, XShift, YShift);
            }
        }
    }

    private void evaluateParticles(Particle p1, final Particle p2, final double rc, double XShift, double YShift) {
        double p1X, p2X, p1Y, p2Y, distance;
        if (p1.equals(p2)) {
            return;
        }
        p1X = p1.getX();
        p2X = p2.getX() + XShift;
        p1Y = p1.getY();
        p2Y = p2.getY() + YShift;

        distance = Math.pow(p1X - p2X, 2) + Math.pow(p1Y - p2Y, 2);

        if (distance < Math.pow(rc + p1.getRadius() + p2.getRadius(), 2)) {
            if (neighborMap.get(p1) != null && neighborMap.get(p2) != null) {
                neighborMap.get(p1).add(p2);
                neighborMap.get(p2).add(p1);
            }
        }

    }

}
