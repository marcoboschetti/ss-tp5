import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by juan on 05/10/16.
 */
public class GranularForce {
    private static final int THREAD_NUMBER = 4; //implements ForceCalculator {
    private final double Kn;
    private final double Kt;
    private final double GRAVITY = -9.81;
    private ExecutorService executorService;
    private calculator[] calculators;
    private Future[] futures;


    public GranularForce(double Kn, double Kt) {
        this.Kn = Kn;
        this.Kt = Kt;
    }


    public void calculateForce(List<Particle> particles, List<Wall> walls, Map<Particle, Set<Particle>> neighborMap, Map<Particle, Vector> forces,
                               boolean calculatePressures) {
        int size = particles.size();

        for (int i = 0; i < size; i++) {
            Particle p1 = particles.get(i);
            Vector force = forces.get(p1);
            force.setX(0);
            force.setY(GRAVITY * 0.01);// FIXME: 07/10/16
            double accumulatedForce = 0;
            for (Particle p2 : neighborMap.get(particles.get(i))) {
                double dist = Math.hypot(p1.getX() - p2.getX(), p1.getY() - p2.getY());
                double overlap = p1.getRadius() + p2.getRadius() - dist;
                if (overlap > 0) {
                    double enx = (p2.getX() - p1.getX()) / dist;
                    double eny = (p2.getY() - p1.getY()) / dist;
//                    double enx = (p2.getY() - p1.getY()) / dist;
//                    double eny = -(p2.getX() - p1.getX()) / dist;

                    double Fn = -Kn * overlap;
                    double Fnx = Fn * enx;
                    double Fny = Fn * eny;
                    double Ft = -Kt * overlap * ((p1.getVelX() - p2.getVelX()) * (-eny) +
                            (p1.getVelY() - p2.getVelY()) * enx);
                    double Ftx = Ft * (-eny);
                    double Fty = Ft * (enx);
                    double Fx = Fnx + Ftx;
                    double Fy = Fny + Fty;

                    if(calculatePressures){
                        accumulatedForce += Math.hypot(Fx, Fy);
                    }

                    force.add(Fx, Fy);
                }
            }

            for (Wall wall : walls) {
                double overlap = wall.getOverlap(p1);
                if (overlap > 0) {
                    Vector normal = wall.getNormal(p1);
                    double enx = normal.getX();
                    double eny = normal.getY();

                    double Fn = -Kn * overlap;
                    double Fnx = Fn * enx;
                    double Fny = Fn * eny;
                    double Ft = -Kt * overlap * (p1.getVelX() * (-eny) +
                            p1.getVelY() * enx);
                    double Ftx = Ft * (-eny);
                    double Fty = Ft * (enx);
                    if(calculatePressures){
                        accumulatedForce += Math.hypot(Fnx + Ftx, Fny + Fty);
                    }

                    force.add(Fnx + Ftx, Fny + Fty);
                }
            }
            if(p1 instanceof Grain){
                ((Grain)p1).setWholePressure(accumulatedForce);
            }
        }
    }

    public void initializeThreads(List<Particle>[][] particlesMatrix,
                                  List<Wall> walls, Map<Particle, Set<Particle>> neighborMap,
                                  Map<Particle, Vector> forces) {
        int width = particlesMatrix.length;
        int height = particlesMatrix[0].length;
        int columnsPerThread = width / THREAD_NUMBER;

        executorService = Executors.newFixedThreadPool(THREAD_NUMBER);
        calculators = new calculator[THREAD_NUMBER];
        futures = new Future[THREAD_NUMBER];
        for(int i = 0; i < THREAD_NUMBER; i++){
            calculators[i] = new calculator(columnsPerThread, i, width, height, particlesMatrix, walls, neighborMap, forces);
        }
    }

    private class calculator implements Runnable {
        private int columnsPerThread, index, width, height;
        private List<Particle>[][] particlesMatrix;
        private Map<Particle, Vector> forces;
        private Map<Particle, Set<Particle>> neighborMap;
        private List<Wall> walls;

        public calculator(int columnsPerThread, int index, int width, int height,
                          List<Particle>[][] particlesMatrix,
                          List<Wall> walls, Map<Particle, Set<Particle>> neighborMap,
                          Map<Particle, Vector> forces) {
            this.columnsPerThread = columnsPerThread;
            this.index = index;
            this.particlesMatrix = particlesMatrix;
            this.height = height;
            this.width = width;
            this.forces = forces;
            this.walls = walls;
            this.neighborMap = neighborMap;

        }

        @Override
        public void run() {
            for (int k = index * columnsPerThread; k < (index + 1) * columnsPerThread && k < width; k++) {
                for (int j = 0; j < height; j++) {
                    for (Particle p1 : particlesMatrix[k][j]) {
                        Vector force = forces.get(p1);
                        force.setX(0);
                        force.setY(GRAVITY * 0.01);// FIXME: 07/10/16
                        for (Particle p2 : neighborMap.get(p1)) {
                            double dist = Math.hypot(p1.getX() - p2.getX(), p1.getY() - p2.getY());
                            double overlap = p1.getRadius() + p2.getRadius() - dist;
                            if (overlap > 0) {
                                double enx = (p2.getX() - p1.getX()) / dist;
                                double eny = (p2.getY() - p1.getY()) / dist;
                                double Fn = -Kn * overlap;
                                double Fnx = Fn * enx;
                                double Fny = Fn * eny;
                                double Ft = -Kt * overlap * ((p1.getVelX() - p2.getVelX()) * (-eny) +
                                        (p1.getVelY() - p2.getVelY()) * enx);
                                double Ftx = Ft * (-eny);
                                double Fty = Ft * (enx);
                                double Fx = Fnx + Ftx;
                                double Fy = Fny + Fty;
                                force.add(Fx, Fy);
                            }
                        }
                        for (Wall wall : walls) {
                            double overlap = wall.getOverlap(p1);
                            if (overlap > 0) {
                                Vector normal = wall.getNormal(p1);
                                double enx = normal.getX();
                                double eny = normal.getY();

                                double Fn = -Kn * overlap;
                                double Fnx = Fn * enx;
                                double Fny = Fn * eny;
                                double Ft = -Kt * overlap * (p1.getVelX() * (-eny) +
                                        p1.getVelY() * enx);
                                double Ftx = Ft * (-eny);
                                double Fty = Ft * (enx);
                                force.add(Fnx + Ftx, Fny + Fty);
                            }
                        }

                    }
                }

            }

        }
    }

    public void calculateForce() {
        for (int i = 0; i < THREAD_NUMBER; i++) {
            futures[i] = executorService.submit(calculators[i]);
        }
        for (Future future : futures) {
            try {
                future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

    }
}
