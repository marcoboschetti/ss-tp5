import java.util.List;

/**
 * Created by tritoon on 21/09/16.
 */
public class ExactOscillation implements IntegrationScheme{

    private double gamma, k;

    public ExactOscillation(double gamma, double k) {
        this.gamma = gamma;
        this.k = k;
    }

    /**
     * deltaT IS NOT A DELTA! ITS TOTAL TIME! I'm really sorry
     * @param toUpdate Particle with x,y,vx,vy from the previous T
     * @param deltaT Time to update the new position and velocity
     * @param forceX new acceleration in X
     * @param forceY new acceleration in Y
     * @param matrix
     * @param sideX
     * @param sideY
     * @param l
     * @param w
     */
    @Override
    public void updateParticle(Particle toUpdate, double deltaT, double forceX, double forceY, List<Particle>[][] matrix, double sideX, double sideY, double l, double w) {
        double m = toUpdate.getMass();
        double newX = Math.exp(-(gamma/(2*m))*deltaT) *
                Math.cos(Math.sqrt(((k/m)-(gamma*gamma/(4*m*m)))) * deltaT);
        toUpdate.setX(newX);
    }

}
